﻿using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("R= ");
            var r = Convert.ToDouble(Console.ReadLine());
            Console.Write("L= ");
            var l = Convert.ToDouble(Console.ReadLine());

            if (r <= 0 || l <= 0)
            {
                Console.WriteLine("Значение не могут быть отрицательными!");
                return;
            }

            Class1 square = new Class1(r, l);
            Console.WriteLine(square.Square_1());
        }
    }
}
