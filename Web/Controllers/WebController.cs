using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using Web.Models;

namespace WebApp.Controllers
{
    public class Webcontroller : Controller
    {
        public IActionResult Index(double R, double L)
        {
            var qq = new WebViewModel();
            qq.L = L;
            qq.R = R;

            try
            {
                qq.mess = (new TRPO_Lab3.Lib.Class1(R, L).Square_1()).ToString();
            }
            catch (ArgumentException error)
            {
                qq.mess = error.Message;
            }
            catch (FormatException error)
            {
                qq.mess = error.Message;
            }
            catch (Exception error)
            {
                qq.mess = error.Message;
            }

            return View(qq);
        }
    }
}

