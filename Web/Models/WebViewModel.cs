using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class WebViewModel
    {
        public string mess { get; set; }
        public double R { get; set; }
        public double L { get; set; }
        public string square { get; set; }
    }
}
