﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Class1
    {
        private double r;
        private double l;

        public Class1(double r, double l)
        {
            this.r = r;
            this.l = l;

            if (r <= 0 || l <= 0)
                throw new ArgumentException();

        }

        public double Square_1()
        {
            return ((this.r * this.l) / 2);
        }
    }
}