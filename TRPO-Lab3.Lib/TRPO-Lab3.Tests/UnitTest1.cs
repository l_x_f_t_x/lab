using System;
using NUnit.Framework;

namespace TRPO_Lab3.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            // �������� �� �������
            const int a = 4;
            const int b = 3;
            const int c = 5;
            const double expected = 6;

            var result = new Lib.Triangle().Square_Triangle(a, b, c);

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void Test2()
        {
            // �������� �� ������������� �������
            const int a = 1;
            const int b = 4;
            const int c = 2;

            TestDelegate action = () => new Lib.Triangle().Square_Triangle(a, b, c);
            Assert.Throws<FormatException>(action);
        }

        [Test]
        public void Test3()
        {
            // �������� �� ������� �������
            const int a = 3;
            const int b = 2;
            const int c = 1;

            TestDelegate action = () => new Lib.Triangle().Square_Triangle(a, b, c);
            Assert.Throws<Exception>(action);
        }

        [Test]
        public void Test4()
        {
            // �������� �� ����������� ��������� �����
            const int a = 0;
            const int b = 2;
            const int c = 3;

            TestDelegate action = () => new Lib.Triangle().Square_Triangle(a, b, c);
            Assert.Throws<ArgumentException>(action);
        }
    }
}