﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPO_Lab3.Wpf
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public MainWindowViewModel()
        { }



        private int _num1;
        private int _num2;
        private int _num3;

        public string total
        {
            get
            {
                try
                {
                    var action = new Lib.Triangle().Square_Triangle(_num1, _num2, _num3);
                    return action.ToString();
                }
                catch (ArgumentException ex)
                {
                    return ex.Message;
                }
                catch (FormatException ex)
                {
                    return ex.Message;
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        public int num1
        {
            get
            {
                return _num1;
            }
            set
            {
                _num1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(total)));
            }
        }

        public int num2
        {
            get
            {
                return _num2;
            }
            set
            {
                _num2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(total)));
            }
        }

        public int num3
        {
            get
            {
                return _num3;
            }
            set
            {
                _num3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(total)));
            }
        }
    }
}
